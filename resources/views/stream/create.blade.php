<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create stream') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @if (sizeof($errors) > 0)
                        {{ $errors }}
                    @endif
                    <form action="{{ route('stream.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <label for="name">Название стрима</label>
                        <input type="text" name="name"  id="name" required>
                        <label for="description">Описание</label>
                        <input type="text" name="description" id="description" required>
                        <label for="preview">Превью</label>
                        <input type="file" name="preview" id="preview" accept=".jpeg,.png,.jpg" required>
                        <input type="submit" value="Создать">
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
