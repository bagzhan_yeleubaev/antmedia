<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Stream list') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="align-items-right flex">
                </div>
                <div class="p-6 bg-white border-b border-gray-200">
                    <p class="font-semibold text-gray-600 text-xl" >{{ $stream->name }}</p>
                    @if ($active)
                        <iframe width="560" height="315" src="{{ "http://89.22.229.228:5080/bagzhanTestApp/play.html?name=" . $stream->stream_id }}" frameborder="0" allowfullscreen></iframe>
                    @else
                        <img src="{{ $stream->preview }}" alt="">
                        <p class="font-semibold text-red-400 text-xm">Стрим на данный момент не активен. Вы можете смотреть только превью</p>
                    @endif
                    
                    <p>{{ $stream->description }}</p>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>