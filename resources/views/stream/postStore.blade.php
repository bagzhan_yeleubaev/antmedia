<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Сохраните эти данные!') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="align-items-right flex">
                </div>
                <div class="p-6 bg-white border-b border-gray-200">
                    <p class="font-semibold text-gray-600 text-xl" >Стрим удачно создан. Пожалуйста, сохраните данные rtmpUrl для того чтобы вставить их в OBS Studio</p>
                    
                    
                    <p>{{ $streamData->rtmpUrl }}</p>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>