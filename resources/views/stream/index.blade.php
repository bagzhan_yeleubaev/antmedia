<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Stream list') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <button class="bg-blue-500 hover:bg-blue-700 font-bold py-2 px-4 rounded-full"> <a href="{{ route('stream.create') }}">Создать</a> </button>
                <div class="align-items-right flex">
                </div>
                <div class="p-6 bg-white border-b border-gray-200">
                    @foreach ($streams as $stream)
                    <div class="p-4 bg-gray border">
                        <a class="font-semibold text-gray-600 text-xl" href="{{ route('stream.show', $stream->id) }}" >{{ $stream->name }}</a>                        
                        <img src="{{ $stream->preview }}" alt="">
                        <p>{{ $stream->description }}</p>
                        <br>
                    </div>
                        
                        <br>
                    @endforeach
                    
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
