<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStreamRequest;
use App\Repository\StreamRepository;
use App\Services\StreamService;

class StreamController extends Controller
{
    private $repository;
    private $service;

    public function __construct()
    {
        $this->repository = new StreamRepository();
        $this->service = new StreamService();
    }

    public function index()
    {

        $streams = $this->repository->getAllStreams();

        return view('stream.index', compact('streams'));
    }

    public function create()
    {
        return view('stream.create');
    }

    public function store(CreateStreamRequest $request)
    {
        $streamData = $this->service->createStream($request->only('name', 'description'));
        $this->repository->createStream($streamData, $request->file('preview'));

        return view('stream.postStore', compact('streamData'));
    }

    public function show($id)
    {
        $stream = $this->repository->getStreamById($id);
        $active = $this->service->isStreamActive($stream->stream_id);

        return view('stream.show', compact('stream', 'active'));
    }
}
