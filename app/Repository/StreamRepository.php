<?php

namespace App\Repository;

use App\DTO\StreamData;
use App\Models\Stream;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class StreamRepository
{

    private $model;

    public function __construct()
    {
        $this->model = Stream::class;
    }

    public function createStream(StreamData $data, UploadedFile $preview)
    {

        $filename = time() . $preview->getClientOriginalName();

        Storage::disk('local')->putFileAs(
            'public/',
            $preview,
            $filename
        );
        $this->model::create(['name' => $data->name, 'stream_id' => $data->stream_id, 'description' => $data->description, 'rtmpUrl' => $data->rtmpUrl, 'preview' => '/storage/' . $filename, 'user_id' => auth()->user()->id]);
    }

    public function getAllStreams()
    {
        return $this->model::all();
    }

    public function getStreamById($id)
    {
        return $this->model::findOrFail($id);
    }
}
