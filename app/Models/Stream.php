<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stream extends Model
{
    protected $table = 'streams';

    protected $fillable = [
        'name',
        'description',
        'preview',
        'rtmpUrl',
        'user_id',
        'stream_id'
    ];
}
