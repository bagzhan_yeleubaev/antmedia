<?php

namespace App\Services;

use App\DTO\StreamData;
use Illuminate\Support\Facades\Http;
use Exception;

class StreamService
{
    const ANT_MEDIA_URL = 'http://89.22.229.228:5080/bagzhanTestApp/rest/v2';



    public function createStream(array $data): StreamData
    {
        $options = array('webRTCViewerLimit' => 5, 'hlsViewerLimit' => 5, 'name' => $data['name'], 'description' => $data['description']);
        $url = self::ANT_MEDIA_URL . '/broadcasts/create';
        $response = Http::acceptJson()->post($url, $options);
        if (!$response->successful()) {
            throw new Exception('Error creating stream');
        }
        $response = $response->json();
        $streamObj = new StreamData($response['streamId'], $response['name'], $response['description'], $response['rtmpURL']);

        return $streamObj;
    }

    public function isStreamActive($streamId): bool
    {
        $url = self::ANT_MEDIA_URL . '/broadcasts/' . $streamId;

        $response = Http::acceptJson()->get($url);
        if (!$response->successful()) {
            throw new Exception($response->getMessage());
        }

        $response = $response->json();
        return $response['status'] == 'broadcasting';
    }
}
