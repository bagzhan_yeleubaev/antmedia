<?

namespace App\DTO;

class StreamData
{
    public function __construct(public string $stream_id, public string  $name, public string $description, public string $rtmpUrl)
    {
    }
}
