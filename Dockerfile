FROM php:8.1 as php

RUN apt-get update -y
RUN apt-get install -y unzip libpq-dev libcurl4-gnutls-dev libzip-dev libmagickwand-dev npm
RUN pecl install imagick
RUN docker-php-ext-install pdo pdo_mysql zip exif
RUN docker-php-ext-enable imagick

WORKDIR /var/www/antmedia
COPY . .

COPY --from=composer:2.2.6 /usr/bin/composer /usr/bin/composer
ENV PORT=8000
ENTRYPOINT [ "Docker/entrypoint.sh" ]