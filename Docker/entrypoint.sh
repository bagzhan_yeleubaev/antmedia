#!/bin/bash

if [ ! -f "vendor/autoload.php" ]; then
    echo "composer not installed"
    composer install
else
    echo "composer installed"
fi

php artisan migrate
php artisan key:generate
php artisan cache:clear
php artisan config:clear
php artisan route:clear

npm install && npm run dev


php artisan serve --port=$PORT --host=0.0.0.0 --env=.env
exec docker-php-entrypoint "$@"
