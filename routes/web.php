<?php

use App\Http\Controllers\StreamController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('stream.index'));
});

Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'stream', 'as' => 'stream.'], function () {
        Route::get('create', [StreamController::class, 'create'])->name('create');
        Route::post('store', [StreamController::class, 'store'])->name('store');
        Route::get('/', [StreamController::class, 'index'])->name('index');
        Route::get('{id}', [StreamController::class, 'show'])->name('show');
    });
});
require __DIR__ . '/auth.php';
