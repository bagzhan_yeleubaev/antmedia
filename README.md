#TestApp

In order to setup project you should have `Docker` installed

`PROJECT SETUP`

```
chmod -R 775 Docker
```

```
docker-compose up --build -d
```

`Launch Project`

Once you built project go to

```
 http://localhost:8000
```
